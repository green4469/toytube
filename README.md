# toytube

Youtube toy project

# Purpose
- Apply TDD, Refactoring
- Test coverage 90%
    - Unit test
    - Integration test
- Setup & use Gitlab CI
    - Sonarqube
    - CD
- Deploy on a local Kubernetes cluster
- Clean code

# Spec
- Kotlin
    - Kluent
- Spring Boot
- RESTful API server
- Use Spring Data JPA, Hibernate
- Gradle multimodule project (toytube-api, toytube-service, ...)
- Swagger API Documentation
- Dockerize app
